#include "ISystem.h"
#include <string>
#pragma once

class FileSystem
{
public:
	inline static FileSystem& Instance()
	{
		static FileSystem instance;
		return instance;
	}

protected:
	void Load(std::string fileName);
	void Initialize();
	void Update();

private:
	friend class GameEngine;
	FileSystem();
	~FileSystem();
	FileSystem(const FileSystem& fileSystem);
	FileSystem& operator =(const FileSystem& fileSystem);
};

