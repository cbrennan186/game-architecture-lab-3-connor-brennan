#include "ISystem.h"
#pragma once

class InputManager
{
public:
	inline static InputManager& Instance()
	{
		static InputManager instance;
		return instance;
	}

protected:
	void Initialize();
	void Update();

private:
	friend class GameEngine;
	InputManager();
	~InputManager();
	InputManager(const InputManager& inputManager);
	InputManager& operator =(const InputManager& inputManager);
};

