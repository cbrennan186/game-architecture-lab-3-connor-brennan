#pragma once

#include "Object.h"
#include <string>

class Component : public Object
{
public:
	void Initialize() override;
	
	virtual void Update();

	virtual const std::string& GetComponentId() { return "Component"; }
	inline static Component& Instance()
	{
		static Component instance;
		return instance;
	}


protected:
	Component();
	~Component();

private:
	friend class GameObject;
};

