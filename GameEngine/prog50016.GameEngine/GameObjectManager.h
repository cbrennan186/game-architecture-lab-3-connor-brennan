#pragma once
#include <list>
#include "GameObject.h"

class GameObjectManager
{
public:
	void AddGameObject(GameObject* component);
	void RemoveGameObject(GameObject* component);
	GameObject* FindGameObjectById(int id);
	inline static GameObjectManager& Instance()
	{
		static GameObjectManager instance;
		return instance;
	}

protected:
	void Initialize();
	void Update();

private:
	std::list<GameObject*> gameObjects();
	GameObjectManager();
	~GameObjectManager();
	GameObjectManager(const GameObjectManager& gameObjectManager);
	GameObjectManager& operator =(const GameObjectManager& gameObjectManager);
	friend class GameEngine;

};

