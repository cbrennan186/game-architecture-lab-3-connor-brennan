#include "ISystem.h"
#include <iostream>

void ISystem::Initialize()
{
	std::cout << "ISystem Created" << std::endl;
}

void ISystem::Update()
{
	std::cout << "ISystem Updated" << std::endl;
}