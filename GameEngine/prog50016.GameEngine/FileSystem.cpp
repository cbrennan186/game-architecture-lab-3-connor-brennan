#include "FileSystem.h"
#include <iostream>

FileSystem::FileSystem()
{

}

FileSystem::~FileSystem()
{
	std::cout << "FileSystem Destroyed" << std::endl;
}

FileSystem::FileSystem(const FileSystem& fileSystem)
{

}

void FileSystem::Initialize()
{
	std::cout << "FileSystem Created" << std::endl;
}

void FileSystem::Load(std::string fileName)
{

}
	
void FileSystem::Update()
{
	std::cout << "FileSystem Updated" << std::endl;
}