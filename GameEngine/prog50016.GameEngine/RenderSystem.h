#include "ISystem.h"
#include <list>
#include "IRenderable.h"
#include <string>
#pragma once

class RenderSystem
{
public:
	void AddRenderable(IRenderable* component);
	void RemoveRenderable(IRenderable* component);
	inline static RenderSystem& Instance()
	{
		static RenderSystem instance;
		return instance;
	}

protected:
	void Initialize();
	void Update();

private:
	friend class GameEngine;
	std::list<IRenderable*> renderComponents;
	std::string name;
	int width;
	int height;
	bool fullscreen;
	void LoadSettings();
	RenderSystem();
	~RenderSystem();
	RenderSystem(const RenderSystem& renderSystem);
	RenderSystem& operator =(const RenderSystem& renderSystem);
};

