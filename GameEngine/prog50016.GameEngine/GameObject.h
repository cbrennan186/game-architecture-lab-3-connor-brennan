#pragma once

#include <string>
#include <map>
#include "Object.h"

class Component;

class GameObject : public Object
{
public: 
    std::map<std::string, Component*> components;

public:
    GameObject();
    ~GameObject();

    void Initialize() override;
    virtual void Update();

    void AddComponent(Component* _component);
    void RemoveComponent(Component* _component);
    inline static GameObject& Instance()
    {
        static GameObject instance;
        return instance;
    }
};

