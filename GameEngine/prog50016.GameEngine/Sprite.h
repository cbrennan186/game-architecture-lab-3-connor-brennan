#include "Component.h"
#include <string>
#include "IRenderable.h"
#pragma once

class Sprite
{
protected:

public:
	Sprite();
	std::string& GetComponentId();
	void Render();
};

