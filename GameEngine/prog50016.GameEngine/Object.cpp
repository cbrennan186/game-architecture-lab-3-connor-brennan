#include "Object.h"
#include <iostream>

Object::Object()
{
	std::cout << "Object Created" << std::endl;
}

Object::~Object()
{
	std::cout << "Object Destroyed" << std::endl;
}

void Object::Initialize()
{
}
