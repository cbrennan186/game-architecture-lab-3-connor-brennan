#include "IRenderable.h"
#include <iostream>

IRenderable::IRenderable()
{
	std::cout << "IRenderable Created" << std::endl;
}

IRenderable::~IRenderable()
{
	std::cout << "IRenderable Destroyed" << std::endl;
}

void IRenderable::Render()
{

}