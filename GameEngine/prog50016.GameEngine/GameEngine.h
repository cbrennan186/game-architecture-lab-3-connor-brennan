#pragma once
#include <iostream>
#include <chrono>
#include <thread>
#include "ISystem.h"
#include "Object.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "Component.h"
#include "GameObject.h"
#include "GameObjectManager.h"

class GameEngine
{
public:
	void Initialize();
	void GameLoop();
	inline static GameEngine& Instance()
	{
		static GameEngine instance;
		return instance;
	}


private:
	GameEngine();
	~GameEngine();
	GameEngine(const GameEngine& gameEngine);
	GameEngine& operator =(const GameEngine& gameEngine);
};