#include "RenderSystem.h"
#include "json.hpp"
#include <fstream>

RenderSystem::RenderSystem()
{
    std::cout << "RenderSystem Created" << std::endl;
}

RenderSystem::~RenderSystem()
{
    std::cout << "RenderSystem Destroyed" << std::endl;
}

RenderSystem::RenderSystem(const RenderSystem& renderSystem)
{

}

void RenderSystem::AddRenderable(IRenderable* component)
{

}

void RenderSystem::RemoveRenderable(IRenderable* component)
{

}

void RenderSystem::Initialize()
{
    std::ifstream inputStream("./Assets/RenderSystem.json");
    std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
    json::JSON document = json::JSON::Load(str);
    std::cout << "File Loaded" << std::endl;
}

void RenderSystem::Update()
{
    std::cout << "Updated Destroyed" << std::endl;
}

void RenderSystem::LoadSettings()
{

}