#include <list>
#include "Asset.h"
#include "ISystem.h"
#pragma once

class AssetManager
{
public:
	void AddAsset(Asset* component);
	void RemoveAsset(Asset* component);
	inline static AssetManager& Instance()
	{
		static AssetManager instance;
		return instance;
	}

protected:
	void Initialize();
	void Update();

private:
	friend class GameEngine;
	std::list<Asset*> assets;
	AssetManager();
	~AssetManager();
	AssetManager(const AssetManager& assetManager);
	AssetManager& operator =(const AssetManager& assetManager);

};

