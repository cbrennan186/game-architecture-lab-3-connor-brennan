#include "GameObjectManager.h"
#include <iostream>

GameObjectManager::GameObjectManager()
{
	std::cout << "GameObjectManager Created" << std::endl;
}

GameObjectManager::~GameObjectManager()
{
	std::cout << "GameObjectManager Destroyed" << std::endl;
}

GameObjectManager::GameObjectManager(const GameObjectManager& gameObjectManager)
{

}

void GameObjectManager::AddGameObject(GameObject* component)
{

}

void GameObjectManager::RemoveGameObject(GameObject* component)
{

}

void GameObjectManager::Initialize()
{

}

void GameObjectManager::Update()
{
	std::cout << "GameObjectManager Updated" << std::endl;
}