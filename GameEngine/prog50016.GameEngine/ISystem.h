#pragma once
class ISystem
{
	friend class GameEngine;

protected:
	void Initialize();
	void Update();

public:
	inline static ISystem& Instance()
	{
		static ISystem instance;
		return instance;
	}
};

