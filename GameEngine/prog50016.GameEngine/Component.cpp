#include <iostream>
#include "Component.h"

Component::Component()
{
	std::cout << "Component Created" << std::endl;
}

Component::~Component()
{
	std::cout << "Component Destroyed" << std::endl;
}

void Component::Initialize()
{
	std::cout << "Component Initialized" << std::endl;
}

void Component::Update()
{
	std::cout << "GameObjectManager Updated" << std::endl;
}
