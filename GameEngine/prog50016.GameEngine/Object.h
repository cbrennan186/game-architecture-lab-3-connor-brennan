#pragma once

#include <string>

class Object
{
public:
	bool initialized = false;
	std::string name = "";
	int id = -1;
	inline static Object& Instance()
	{
		static Object instance;
		return instance;
	}

public:
	virtual void Initialize();

	bool IsInitialized() { return initialized; }
	std::string GetName() { return name; }
	int GetId() { return id; }

protected:
	Object();
	virtual ~Object();
};

