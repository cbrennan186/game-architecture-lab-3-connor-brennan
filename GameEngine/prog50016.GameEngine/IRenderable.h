#pragma once
class IRenderable
{
	friend class RenderSystem;

protected:
	IRenderable();
	~IRenderable();
	void Render();

};

