#include "GameEngine.h"


void GameEngine::Initialize()
{
	ISystem::Instance().Initialize();
	Object::Instance().Initialize();
	RenderSystem::Instance().Initialize();
	FileSystem::Instance().Initialize();
	InputManager::Instance().Initialize();
	AssetManager::Instance().Initialize();
	Component::Instance().Initialize();
	GameObject::Instance().Initialize();
	GameObjectManager::Instance().Initialize();

}

void GameEngine::GameLoop()
{
	std::chrono::time_point<std::chrono::system_clock> _time;
	std::chrono::duration<float> deltaTime(0);
	std::chrono::duration<float> totalTime(0);

	while(true)
	{
		ISystem::Instance().Update();
		RenderSystem::Instance().Update();
		FileSystem::Instance().Update();
		InputManager::Instance().Update();
		AssetManager::Instance().Update();
		Component::Instance().Update();
		GameObject::Instance().Update();
		GameObjectManager::Instance().Update();

		_time = std::chrono::system_clock::now();

		// We need to do something
		std::this_thread::sleep_for(std::chrono::milliseconds(16));

		deltaTime = std::chrono::system_clock::now() - _time;

		totalTime += deltaTime;
		std::cout << totalTime.count() << std::endl;

		if (totalTime.count() > 5.0f)
		{
			break;
		}
	}
}

GameEngine::GameEngine()
{
	std::cout << "GameEngine Created" << std::endl;
}

GameEngine::~GameEngine()
{
	std::cout << "GameEngine Destroyed" << std::endl;
}
	
GameEngine::GameEngine(const GameEngine& gameEngine)
{
	
}