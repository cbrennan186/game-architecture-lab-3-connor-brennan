#include <iostream>
#include "GameObject.h"
#include "Component.h"

GameObject::GameObject()
{
    std::cout << "GameObject Created" << std::endl;
}

GameObject::~GameObject()
{
    std::cout << "GameObject Destroyed" << std::endl;
}

void GameObject::Initialize()
{
    std::cout << "GameObject Initialized" << std::endl;
}

void GameObject::Update()
{
    std::cout << "GameObject Updated" << std::endl;
    for (auto comp : components)
    {
        comp.second->Update();
    }
}

void GameObject::AddComponent(Component* _component)
{
    std::cout << "Add Component, needs to be added to the map" << std::endl;
}

void GameObject::RemoveComponent(Component* _component)
{
    std::cout << "Remove Component, needs to be remove from the map" << std::endl;
}
